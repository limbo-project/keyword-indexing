# KINDEX - Automatic keyword indexing with knowledge graph services

This tool assigns keyword URIs from the [German Authority File (GND)](https://www.dnb.de/DE/Professionell/Standardisierung/GND/gnd_node.html) to text snippets by combining the following webservices from the web of data:
* [DBPedia Spotlight](https://www.dbpedia-spotlight.org/)
* [DBpedia](http://dbpedia.org/sparql)
* [DBpedia Lookup](https://global.dbpedia.org/same-thing/lookup/)
* [Wikidata](https://query.wikidata.org/sparql)
* [LOBID (HBZ)](https://lobid.org/)

The indexing approach is based on a combination of typical indexing techniques, such as:

* **Named entity Recognition & Entity Disambiguation** (done by DBPedia Spotlight)
* **Lexical Indexing** (e.g., preferred and alternative labels in GND) 

but also utilizes identity links (e.,g., owl:sameAs, skos:exactMatch) that exist in the web of data (i.e., DBpedia to Wikidata, Wikidata to GND) to facilitate:

* **Identity Matching**

The schematic workflow of the script is depicted below: 

![alt text](https://gitlab.com/limbo-project/keyword-indexing/raw/master/img/kindex.png "Kindex Workflow")

## Requirements 

These tools need to be installed on your system in order for the script to work:

* curl
* jq
* [SparqlIntegrate](https://github.com/SmartDataAnalytics/SparqlIntegrate)

## Required Command Line Arguments
* `-c` a turtle file in DCAT format  (e.g. `catalog.all.ttl`)  
* `-d` a dataset URI that is contained in the DCAT catalog (e.g. `https://metadata.limbo-project.org/dataset-Stationsdaten-RNI--1155351498`) with text descriptions as metadata (e.g., via the properties `dct:title` and `dct:description`)
* `-k` the filename to which RDF triples with assigned keywords should be written (e.g. `keyword.ttl`)
