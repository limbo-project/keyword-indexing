# Indexing Econstor publications with knowledge graph services

Econstor publications can be indexed by finding the descriptors from the STW Thesaurus for Economics, which match DBpedia URIs as identified by Spotlight. In some cases, the STW descriptor might not be linked to a DBpedia resource directly but can be determined by following the identity links that exist between DBpedia and Wikidata. This is realized through accessing the DBPedia same-thing lookup service as it is shown in the bash snippet below.

```console
matchWikidata=1 
match=`curl -X GET -G $dbpediaLookupAPI --data-urlencode url=$dbpediaURL | jq -r '.locals[0]'`  
if [ -z "$match" ]; then 
    getDescriptor
fi 
if [ -z "$descriptor" ]; then
    match=`curl -X GET -G $dbpediaAPI --data-urlencode query=$owlSameAsQuery -H "Accept: application/sparql-results+json" | jq -r '.results.bindings[0].o.value'`
    if [ -z "$match" ]; then    
        matchWikidata=0
        getDescriptor
    fi
fi
```

When there exists a mapping, the *getDescriptor* function determines the corresponding STW descriptor of the ![STW-to-Wikidata mapping collection](http://zbw.eu/stw/version/latest/mapping/wikidata/about.de.html). In case there is no link, the procedure sends a HTTP request to the DBpedia SPARQL endpoint to determine whether there exist any mappings to the GND.

**owlSameAsQuery**
```sql
PREFIX owl: <http://www.w3.org/2002/07/owl#> 
PREFIX wdt: <http://www.wikidata.org/prop/direct/> 

SELECT DISTINCT ?o 
WHERE {
    <$dbpediaURL> owl:sameAs ?o . 
    FILTER regex(str(?o),"http://d-nb.info/gnd/")} 
```

Afterward, the cross-concordance that exist between the GND and the STW can be obained by querying the ![STW-to-GND mapping](http://zbw.eu/stw/version/latest/download/about.de.html) from an in-memory model that is accessed with the help of ![SparqlIntegrate](https://github.com/SmartDataAnalytics/SparqlIntegrate).

Upon obtaining a match, the *getDescriptor* function is called, which queries the appropriate mapping collection for a suitable STW descriptor.

```console
getDescriptor () {
    file="stw_wikidata_mapping.ttl"
    if [ "$matchWikidata" -eq 0 ]; then
        file="stw_gnd_mapping.ttl"
    fi
    sed -e "s#\$MAPPING#$match#" skosExactMatchQuery.sparql > mapping.sparql 
    stw=`sparql-integrate "$file" --jq mapping.sparql | jq -r '.[0].o.id'`
}

```

**skosExactMatchQuery**
```sql
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
SELECT DISTINCT ?o 
WHERE { 
    ?o skos:exactMatch <$MAPPING> . 
    FILTER regex(str(?o),"http://zbw.eu/stw/descriptor/") 
} 
```
The same procedure is carried out for each of the Spotlight annotations. It stops as soon as the respective STW descriptor is found. Once the set of relevant KOS descriptors has been obtained, they can be assigned to a metadata catalogue in RDF format by applying a SPARQL UPDATE command that is customised with SparqlIntegrate.

```console
if [ ! -z "$stw" ]; then 
    sparql-integrate --io=catalog.ttl updateCatalogQuery.sparql spo.sparql
fi
```

**updateCatalogQuery**
```sql
PREFIX econstor: <http://linkeddata.econstor.eu/beta/resource/publications/> 
PREFIX dc: <http://purl.org/dc/elements/1.1/> 

INSERT { 
    ?paper dct:subject ?subject .
}
WHERE { 
    ?paper a swc:Paper, sioc:Item, foaf:Document .
    FILTER (str(?paper) = sys:getenv('PAPER'))
	  BIND(URI(sys:getenv('KEYWORD')) as ?subject) 
}
``` 